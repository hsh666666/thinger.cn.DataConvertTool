# thinger.cn.DataConvertTool

#### 介绍
上位机数据转换助手及转换库

#### 软件架构
C#


#### 安装教程

1.  Nuget搜索thinger.DataConvertLib

#### 使用说明

1.  安装最新版本4.0.1
2.  添加using thinger.DataConvertLib

#### 如有疑问

添加微信：thinger002，备注来自Gitee